//
//  MenuView.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class MenuView: UIView {
	
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var topButton: UIButton!
	@IBOutlet weak var BottomButton: UIButton!
	
	func setUpButtonsStyle(){
		topButton.layer.borderColor 	= UIColor.white.cgColor
		BottomButton.layer.borderColor	= UIColor.white.cgColor
		topButton.layer.borderWidth 	= 2
		BottomButton.layer.borderWidth	= 2
	}
	
	func changeButtonsToDifficultySelection(){
		backButton.isHidden = false
		topButton.setTitle("Moderate", for: .normal)
		BottomButton.setTitle("Impossible", for: .normal)
	}
	
	func changeButtonsToModeSelection(){
		backButton.isHidden = true
		topButton.setTitle("1v1", for: .normal)
		BottomButton.setTitle("Solo", for: .normal)
	}
}
