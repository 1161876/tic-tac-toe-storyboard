//
//  GameView.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class GameView: UIView {
	
	@IBOutlet weak var boardImage: UIImageView!
	@IBOutlet weak var scoreCircleImage: UIImageView!
	@IBOutlet weak var scoreCrossImage: UIImageView!
	@IBOutlet weak var winnerImage: UIImageView!
	
	@IBOutlet weak var modeButton: UIButton!
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var newGameButton: UIButton!
	
	@IBOutlet weak var winnerLabel: UILabel!
	@IBOutlet weak var circleScoreLabel: UILabel!
	@IBOutlet weak var crossScoreLabel: UILabel!
	@IBOutlet weak var modeLabel: UILabel!
	
	@IBOutlet weak var boardWidhtConstraint: NSLayoutConstraint!
	@IBOutlet weak var boardHightConstraint: NSLayoutConstraint!

	@IBOutlet weak var gameCollectionView: UICollectionView!

	@IBOutlet weak var aIIndicatorView: NVActivityIndicatorView!
	
	func setColorMode(isDarkMode: Bool){
		var modeColor = UIColor.white
		
		if isDarkMode{
			scoreCircleImage.image = Images.darkModeCircle
			scoreCrossImage.image  = Images.darkModeCross
			self.backgroundColor = UIColor.init(red: 61/255, green: 56/255, blue: 56/255, alpha: 1)
			boardImage.tintColor = UIColor.init(red: 125/255, green: 118/255, blue: 119/255, alpha: 1)
		}else{
			modeColor = UIColor.black
			scoreCircleImage.image 	= Images.lightModeCircle
			scoreCrossImage.image 	= Images.lightModeCross
			boardImage.tintColor	= modeColor
			self.backgroundColor = UIColor.white
		}
		
		modeLabel.textColor			= modeColor
		backButton.tintColor 		= modeColor
		circleScoreLabel.tintColor 	= modeColor
		crossScoreLabel.tintColor 	= modeColor
		circleScoreLabel.textColor 	= modeColor
		crossScoreLabel.textColor 	= modeColor
		winnerLabel.textColor 		= modeColor
		backButton.tintColor 		= modeColor
		aIIndicatorView.color 		= modeColor
		
		newGameButton.setTitleColor(modeColor, for: .normal)
		winnerImage.image = winnerImage.image?.changeToOppositeMode()
		
		self.gameCollectionView.reloadData()
	}
	
	func setupGameView(){
		setGameConstraints()
		newGame()
		layoutCells()
		setupIndicatorView()
	}
	
	func setupIndicatorView(){
		self.aIIndicatorView.isHidden = false
		self.aIIndicatorView.type = .ballPulse
	}
	
	private func setGameConstraints(){
		boardWidhtConstraint.constant = self.frame.size.width
		boardHightConstraint.constant = self.frame.size.width
	}
	
	func newGame(){
		winnerImage.isHidden = true
		winnerLabel.isHidden = true
		gameCollectionView.reloadData()
	}
	
	func setGameMode(gameMode: GameMode){
		modeLabel.text = gameMode.rawValue
	}
	
	func setWinner(winner: Player?, isDarkMode: Bool){
		
		if winner == nil{
			winnerLabel.isHidden = false
			winnerImage.isHidden = true
			winnerLabel.text = "It's a Draw"
			return
		}
		
		winnerLabel.isHidden = false
		winnerImage.isHidden = false
		winnerLabel.text = "The Winner Is:"
		winnerImage.image = PlayerImage.get(player: winner!, isDarkMode: isDarkMode)
		
		gameCollectionView.reloadData()
	}
	
	func updateScore(crossScore: String, circleScore: String){
		circleScoreLabel.text = circleScore
		crossScoreLabel.text = crossScore
	}
	
	func showYourTurn(isYourTurn: Bool){
		if isYourTurn{
			winnerLabel.isHidden = false
			winnerImage.isHidden = true
			winnerLabel.text = "It's Your Turn"
			return
		}
		winnerLabel.isHidden = true
		winnerImage.isHidden = true
	}
	
	
	private func layoutCells() {
		let layout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 0, right: 2)
		layout.minimumInteritemSpacing = 10
		layout.minimumLineSpacing = 10
		layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 35)/3, height: ((UIScreen.main.bounds.size.width - 35)/3))
		gameCollectionView!.collectionViewLayout = layout
	}
	
}
