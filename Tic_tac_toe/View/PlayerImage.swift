//
//  PlayerImage.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class PlayerImage{
	class func get(player: Player, isDarkMode: Bool) -> UIImage{
		if player == Player.aI{
			if isDarkMode{
				return Images.darkModeCross
			}
			return Images.lightModeCross
		}else{
			if isDarkMode{
				return Images.darkModeCircle
			}
			return Images.lightModeCircle
		}
	}
}
