//
//  GameCollectionViewCell.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 28/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var backgroundLayer: UIView!
	@IBOutlet weak var cellImage: UIImageView!

	func setCellImage(image: UIImage){
		cellImage.image = image
	}
	
	func setCellBackgroundColor(isHighLighted: Bool, isDarkMode: Bool){
		if !isHighLighted{
			backgroundLayer.backgroundColor = .clear
			return
		}
		if isDarkMode{
			backgroundLayer.backgroundColor = Color.darkGreen
		}else{
			backgroundLayer.backgroundColor = Color.lightGreen
		}
	}
}
