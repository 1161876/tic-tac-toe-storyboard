//
//  SceneDelegate.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 27/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
	var window: UIWindow?


	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		guard let _ = (scene as? UIWindowScene) else { return }
	}
}

