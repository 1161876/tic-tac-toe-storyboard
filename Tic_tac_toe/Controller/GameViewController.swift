//
//  ViewController.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 27/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//
import UIKit

class GameViewController: UIViewController, GameSubscriber{
	
	@IBOutlet var gameView: GameView!
	private var game: Game!
	var isDarkMode = false
	var gameMode = GameMode.solo
	private var isGameInProgress = true
	private var isAITurn = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		game = Game(subscriber: self)
		gameView.setGameMode(gameMode: gameMode)
		newGame()
		setButtonsTarget()
		registerGameCell()
		setupGameView()
		isDarkMode = Settings.isDarkMode()
		setColorMode()
	}
	
	private func setButtonsTarget(){
		gameView.backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
		gameView.modeButton.addTarget(self, action: #selector(modeButtonPressed), for: .touchUpInside)
		gameView.newGameButton.addTarget(self, action: #selector(newGameButtonPressed), for: .touchUpInside)
	}
	
	@objc func modeButtonPressed(_ sender: UIButton) {
		isDarkMode = !isDarkMode
		setColorMode()
	}

	private func setColorMode(){
		Settings.setMode(isDarkMode: isDarkMode)
		gameView.setColorMode(isDarkMode: isDarkMode)
		setDarkModeButtonImage(isDarkMode: !isDarkMode)
		gameView.gameCollectionView.reloadData()
	}
	
	func didUpdateScore(crossScore: String, circleScore: String) {
		gameView.updateScore(crossScore: crossScore, circleScore: circleScore)
	}
	
	func didFoundWinner(winner: Player?) {
		isGameInProgress = false
		gameView.setWinner(winner: winner, isDarkMode: isDarkMode)
	}
	
	private func setDarkModeButtonImage(isDarkMode: Bool){
		if isDarkMode{
			gameView.modeButton.setImage(Images.darkModeIcon, for: .normal)
			gameView.modeButton.tintColor = UIColor.black
		}else{
			gameView.modeButton.setImage(Images.lightModeIcon, for: .normal)
			gameView.modeButton.tintColor = UIColor.white
		}
	}
	
	@objc func backButtonPressed(_ sender :UIButton) {
		performSegue(withIdentifier: "gameToMenu", sender: self)
	}
	
	@objc func newGameButtonPressed(_ sender: UIButton) {
		isGameInProgress = true
		newGame()
	}
	
	private func newGame(){
		self.game.newGame()
		gameView.newGame()
		checkAITurn()
	}
	
	private func showYourTurnIfNeeded(){
		if !game.isAIMove() && gameMode != GameMode.solo && game.getWinner() == nil{
			gameView.showYourTurn(isYourTurn: true)
			return
		}
		gameView.showYourTurn(isYourTurn: false)
	}
	
	private func registerGameCell(){
		let nib = UINib(nibName: "GameCollectionViewCell", bundle: nil)
		gameView.gameCollectionView.register(nib, forCellWithReuseIdentifier: "GameCell")
	}

	
	private func setNoSelectionTime(){
		gameView.gameCollectionView.allowsSelection = false
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			self.gameView.aIIndicatorView.stopAnimating()
			self.gameView.gameCollectionView.allowsSelection = true
		}
	}
	
	private func checkAITurn(){
		if isGameInProgress{
			showYourTurnIfNeeded()
		}
		if gameMode == GameMode.solo || !game.isAIMove() || !isGameInProgress{
			return
		}
		gameView.aIIndicatorView.startAnimating()
		gameView.gameCollectionView.allowsSelection = false
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			let aIMove = self.getAIMove()
			self.gameView.gameCollectionView.allowsSelection = true
			self.gameView.gameCollectionView.selectItem(at: IndexPath(item: aIMove, section: 0), animated: true, scrollPosition: .bottom)
			self.collectionView(self.gameView.gameCollectionView, didSelectItemAt: IndexPath(item: aIMove, section: 0))
		}
	}
	
	private func getAIMove() -> Int{
		if gameMode == GameMode.impossible{
			return UnbeatableAI().getNextMove(gameField: self.game.getGameField())
		}
		return ModerateAI().getNextMove(gameField: self.game.getGameField())
	}
	
	private func setupGameView(){
		gameView.setupGameView()
	}
}

extension GameViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 9
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCell", for: indexPath) as! GameCollectionViewCell
		let player = game.getPositionPlayer(index: indexPath.row)
		if player == nil{
			cell.setCellImage(image: UIImage())
		}else{
			cell.setCellImage(image: PlayerImage.get(player: player!, isDarkMode: isDarkMode))
		}
		cell.setCellBackgroundColor(isHighLighted: game.isCellHighlighted(index: indexPath.row), isDarkMode: isDarkMode)
		return cell
	}
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		gameView.aIIndicatorView.stopAnimating()
		if !isGameInProgress {return}
		setNoSelectionTime()
		game.setCurrentPlayerSelection(index: indexPath.row)
		gameView.gameCollectionView.reloadData()
		checkAITurn()
	}
}
