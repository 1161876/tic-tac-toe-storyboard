//
//  MenuViewController.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 30/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

	@IBOutlet var menuView: MenuView!
	private var selectedGameMode = GameMode.solo
	private var isGameModeSelected = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		menuView.setUpButtonsStyle()
		menuView.changeButtonsToModeSelection()
		setButtonsTarget()
	}
	
	private func setButtonsTarget(){
		menuView.backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
		menuView.topButton.addTarget(self, action: #selector(topButtonPressed), for: .touchUpInside)
		menuView.BottomButton.addTarget(self, action: #selector(bottomButtonPressed), for: .touchUpInside)
	}
	
	@objc func backButtonPressed(_ sender: UIButton) {
		menuView.changeButtonsToModeSelection()
	}
	
	@objc func topButtonPressed(_ sender: UIButton) {
		if isGameModeSelected{
			selectedGameMode = GameMode.moderate
			performSegue(withIdentifier: "menuToGame", sender: self)
		}else{
			selectedGameMode = GameMode.solo
			performSegue(withIdentifier: "menuToGame", sender: self)
		}
	}
	
	@objc func bottomButtonPressed(_ sender: UIButton) {
		if !isGameModeSelected{
			isGameModeSelected = true
			menuView.changeButtonsToDifficultySelection()
		}else{
			selectedGameMode = GameMode.impossible
			performSegue(withIdentifier: "menuToGame", sender: self)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "menuToGame"{
			let destVC = segue.destination as! GameViewController
			destVC.gameMode = selectedGameMode
		}
	}
}
