//
//  Settings.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 09/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//
import Foundation

class Settings {
	
	class func isDarkMode() -> Bool{
		let isDarkMode = UserDefaults.standard.bool(forKey: "isDarkMode")
		if isDarkMode == true{
			return true
		}else{
			return false
		}
	}
	
	class func setMode(isDarkMode: Bool){
		UserDefaults.standard.set(isDarkMode, forKey: "isDarkMode")
	}
}
