//
//  Constants.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

struct Images{
	//Image constants assets
	static let appIcon			= UIImage(named:"icon")!
	static let darkModeIcon		= UIImage(named:"dark")!
	static let lightModeIcon	= UIImage(named:"light")!
	static let leftArrow		= UIImage(named:"leftArrow")!
	static let gameBoard		= UIImage(named:"tic_tac_toe_board")!
	static let lightModeCircle	= UIImage(named:"tic_tac_toe_circle")!
	static let darkModeCircle	= UIImage(named:"tic_tac_toe_circle_darkMode")!
	static let lightModeCross	= UIImage(named:"tic_tac_toe_cross")!
	static let darkModeCross	= UIImage(named:"tic_tac_toe_cross_darkMode")!
}

struct Color {
	//Color constants
	static let red				= UIColor(red: 224/255, green: 24/255, blue: 41/255, alpha: 1)
	static let white			= UIColor.white
	static let darkGray			= UIColor.init(red: 61/255, green: 56/255, blue: 56/255, alpha: 1)
	static let lightGray		= UIColor.init(red: 125/255, green: 118/255, blue: 119/255, alpha: 1)
	static let black			= UIColor.black
	static let lightGreen		= UIColor.init(red: 226/255, green: 240/255, blue: 217/255, alpha: 1)
	static let darkGreen		= UIColor.init(red: 50/255, green: 77/255, blue: 35/255, alpha: 1)
}

struct NotificationName{
	static let backButtonPressed = Notification.Name("tiagosantos.ios.backButtonPressed")
	static let modeButtonPressed = Notification.Name("tiagosantos.ios.modeButtonPressed")
}

