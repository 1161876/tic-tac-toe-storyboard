//
//  UIImage.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 08/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

extension UIImage{
	func changeToOppositeMode() -> UIImage{
		switch self {
		case Images.darkModeCircle:
			return Images.lightModeCircle
		case Images.lightModeCircle:
			return Images.darkModeCircle
		case Images.darkModeCross:
			return Images.lightModeCross
		case Images.lightModeCross:
			return Images.darkModeCross
		default:
			return self
		}
	}
}
