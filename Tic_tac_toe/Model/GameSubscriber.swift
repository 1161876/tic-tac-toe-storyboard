//
//  GameDelegate.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

protocol GameSubscriber{
	func didFoundWinner(winner: Player?)
	func didUpdateScore(crossScore: String, circleScore: String)
}

