//
//  File.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 12/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

enum Player: Int{
	case aI = 1
	case human = 4
}
