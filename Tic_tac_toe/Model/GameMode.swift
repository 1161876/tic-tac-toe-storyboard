//
//  GameMode.swift
//  Tic_tac_toe
//
//  Created by Tiago  Santos on 07/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

enum GameMode: String{
	case solo = "1v1"
	case moderate = "Moderate"
	case impossible = "Impossible"
}
