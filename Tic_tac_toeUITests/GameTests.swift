//
//  GameTests.swift
//  Tic_tac_toeUITests
//
//  Created by Tiago  Santos on 11/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class GameTests: XCTestCase {
	
	let app =  XCUIApplication()
	let firstButton = XCUIApplication().buttons["menuFirstButton"]
	let secondButton = XCUIApplication().buttons["menuSecondButton"]
	
	override func setUp() {
        super.setUp()
		UserDefaults.standard.set(false, forKey: "isDarkMode")
        continueAfterFailure = false
        app.launch()
		app/*@START_MENU_TOKEN@*/.buttons["menuFirstButton"]/*[[".otherElements[\"menuView\"]",".buttons[\"1v1\"]",".buttons[\"menuFirstButton\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
    }
	
	
	func testCrossAsWinner(){
		let selectionSequence = [0,1,3,4,6]
		
		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let winnerImage = app.images["gameWinnerImage"]
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "The Winner Is:")
		XCTAssert(winnerImage.exists)
		XCTAssertEqual(gameCircleScore, "0")
		XCTAssertEqual(gameCrossScore, "1")
	}
	
	func testCircleAsWinner(){
		let selectionSequence = [1,0,2,4,7,8]

		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let winnerImage = app.images["gameWinnerImage"]
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "The Winner Is:")
		XCTAssert(winnerImage.exists)
		XCTAssertEqual(gameCircleScore, "1")
		XCTAssertEqual(gameCrossScore, "0")
	}
	
	func testDraw(){
		let selectionSequence = [0,1,3,4,7,6,2,5,8]

		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "It's a Draw")
		XCTAssertEqual(gameCircleScore, "0")
		XCTAssertEqual(gameCrossScore, "0")
	}
	
	func testBackButton(){
		XCTAssert(app.otherElements["gameView"].isHittable)
		XCUIApplication().buttons["gameBackButton"].tap()
		XCTAssert(app.otherElements["menuView"].isHittable)
	}
	
	func testDarkModeButton(){
		let selectionSequence = [0,1,3,4,7,6,2,5,8]
		selectPlayersMove(selectionSequence: selectionSequence)
		
		XCTAssertTrue(app.buttons["dark"].isHittable)
		app/*@START_MENU_TOKEN@*/.buttons["dark"]/*[[".otherElements[\"gameView\"].buttons[\"dark\"]",".buttons[\"dark\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
		XCTAssertTrue(app.buttons["light"].isHittable)
		app/*@START_MENU_TOKEN@*/.buttons["light"]/*[[".otherElements[\"gameView\"].buttons[\"light\"]",".buttons[\"light\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
	}
	
	private func selectPlayersMove(selectionSequence: [Int]){
		let collectionViewsQuery = app/*@START_MENU_TOKEN@*/.collectionViews/*[[".otherElements[\"gameView\"].collectionViews",".collectionViews"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
		
		for selection in selectionSequence{
			collectionViewsQuery.children(matching: .cell).element(boundBy: selection).children(matching: .other).element.children(matching: .other).element.tap()
			usleep(100000)
		}
	}
}
