//
//  Tic_tac_toeUITests.swift
//  Tic_tac_toeUITests
//
//  Created by Tiago  Santos on 27/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class MenuTests: XCTestCase {

	let app =  XCUIApplication()
	let firstButton = XCUIApplication().buttons["menuFirstButton"]
	let secondButton = XCUIApplication().buttons["menuSecondButton"]
	
	override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
	
	func testMenuAppearCorrectly(){
		XCTAssertEqual(firstButton.label , "1v1")
		XCTAssertEqual(secondButton.label, "Solo")
	}
	
	func testSelectMultiplePlayerSelection(){
		secondButton.tap()
		XCTAssertTrue(app.buttons["menuBackButton"].isHittable)
		XCTAssertEqual(firstButton.label , "Moderate")
		XCTAssertEqual(secondButton.label, "Impossible")
	}
	
	func testSinglePlayerSelection(){
		XCUIApplication()/*@START_MENU_TOKEN@*/.buttons["menuFirstButton"]/*[[".otherElements[\"menuView\"]",".buttons[\"1v1\"]",".buttons[\"menuFirstButton\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
		XCTAssert(app.otherElements["gameView"].isHittable)
		let modeText = app.staticTexts["gameModeLabel"].label
		XCTAssertEqual(modeText, "1v1")
	}
	
	func testBackButtonResetButtonValues(){
		secondButton.tap()
		XCTAssertEqual(firstButton.label , "Moderate")
		XCTAssertEqual(secondButton.label, "Impossible")
		app.buttons["menuBackButton"].tap()
		XCTAssertEqual(firstButton.label , "1v1")
		XCTAssertEqual(secondButton.label, "Solo")
	}
	
	func testModerateAISelection(){
		secondButton.tap()
		XCUIApplication().buttons["menuFirstButton"].tap()
		let modeText = app.staticTexts["gameModeLabel"].label
		XCTAssertEqual(modeText, "Moderate")
	}
	
	func testImpossibleAISelection(){
		secondButton.tap()
		XCUIApplication().buttons["menuSecondButton"].tap()
		let modeText = app.staticTexts["gameModeLabel"].label
		XCTAssertEqual(modeText, "Impossible")
	}
}
