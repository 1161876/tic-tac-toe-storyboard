//
//  GameViewControllerTests.swift
//  Tic_tac_toeTests
//
//  Created by Tiago  Santos on 12/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class GameViewControllerTests: XCTestCase {

	let gameVC = UIStoryboard(name: "Main", bundle: nil)
	.instantiateViewController(withIdentifier: "gameViewController") as! GameViewController
	
	override func setUp() {
		super.setUp()
		//Make sure that the userDefaults value is set to false to force Light mode
		UserDefaults.standard.set(false, forKey: "isDarkMode")
		gameVC.beginAppearanceTransition(true, animated: false)
		gameVC.endAppearanceTransition()
    }
	
	//test light mode
	func testLightMode(){
		XCTAssertFalse(gameVC.isDarkMode)
	}
	
	//test dark mode
	func testDarkMode(){
		UserDefaults.standard.set(false, forKey: "isDarkMode")
		gameVC.gameView.modeButton.sendActions(for: .touchUpInside)
		XCTAssertTrue(gameVC.isDarkMode)
	}
}
