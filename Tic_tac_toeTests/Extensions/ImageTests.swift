//
//  ImageTests.swift
//  Tic_tac_toeTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class ImageTests: XCTestCase {

	func testSwitchFromCrossLightModeToCrossDarkMode(){
		XCTAssertEqual(Images.lightModeCross.changeToOppositeMode(), Images.darkModeCross)
	}

	func testSwitchFromCrossDarkModeToCrossLightMode(){
		XCTAssertEqual(Images.darkModeCross.changeToOppositeMode(), Images.lightModeCross)
	}
	
	func testSwitchFromCircleLightModeToCircleDarkMode(){
		XCTAssertEqual(Images.lightModeCircle.changeToOppositeMode(), Images.darkModeCircle)
	}

	func testSwitchFromCircleDarkModeToCircleLightMode(){
		XCTAssertEqual(Images.darkModeCircle.changeToOppositeMode(), Images.lightModeCircle)
	}
	
	func testFunctionHasNoEffectInUnsupportedImages(){
		XCTAssertEqual(Images.appIcon.changeToOppositeMode(), Images.appIcon)
	}
}
